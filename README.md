# Botón de Emergencia

Proyecto de Botón de Emergencia contra Violencia de Género basado en Flutter.

## Objetivo

Ofrecer un método de respuesta rápido, intuitivo y efectivo en contra de los casos de Violencia de Género.
