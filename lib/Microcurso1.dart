import 'package:flutter/material.dart';
import 'microcurso1text/microcurso1text.dart';
import 'microcurso1text/microcurso1test.dart';

class Microcurso1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Empoderamiento de la Mujer'),
      ),
      body: PageView(
        children: [
          Microcurso1Intro(),
          Microcurso1Cap1(),
          Microcurso1Cap11(),
          Microcurso1Cap2(),
          Image.asset('assets/Picture1.png', fit: BoxFit.fill,),
          Image.asset('assets/Picture2.png', fit: BoxFit.fill,),
          Image.asset('assets/Picture3.png', fit: BoxFit.fill,),
          Image.asset('assets/Picture4.png', fit: BoxFit.fill,),
          Image.asset('assets/Picture5.png', fit: BoxFit.fill,),
          Microcurso1Cap3(),
          Microcurso1Cap31(),
          Microcurso1Cap32(),
          Microcurso1Cap33(),
          Microcurso1Cap4(),
          Microcurso1Cap5(),
          Microcurso1Test()
        ],
        scrollDirection: Axis.horizontal,
      )
    );
  }
}

