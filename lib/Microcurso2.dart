import 'package:flutter/material.dart';
import 'microcurso2text/microcurso2text.dart';
import 'microcurso2text/microcurso2test.dart';

class Microcurso2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Violencia de Género'),
      ),
      body: PageView(
        children: [
          Microcurso2Intro(),
          Microcurso2Cap1(),
          Microcurso2Cap11(),
          Microcurso2Cap12(),
          Microcurso2Cap12_2(),
          Microcurso2Cap12_3(),
          Microcurso2Cap12_4(),
          Microcurso2Cap13(),
          Microcurso2Cap13_2(),
          Microcurso2Cap2(),
          Microcurso2Cap2_2(),
          Microcurso2Cap21(),
          Microcurso2Cap21_2(),
          Microcurso2Cap22(),
          Microcurso2Cap23(),
          Microcurso2Cap3(),
          Microcurso2Cap4(),
          Microcurso2Cap5(),
          Microcurso2Cap6(),
          Microcurso2Test()
        
        ],
        scrollDirection: Axis.horizontal,
      )
    );
  }
}
