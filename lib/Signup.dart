import 'package:boton_panico/firebase_database_util.dart';
import 'package:boton_panico/main.dart';
import 'package:boton_panico/models/denuncia.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;


class Signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Simple Login Demo',
      theme: new ThemeData(
        primaryColor: Colors.red
      ),
      home: new LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType {
  login,
  register
}

class _LoginPageState extends State<LoginPage> {
  String _selectedGender;
  bool _success;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<String> _genders = ['Hombre', 'Mujer'];
  static DateTime selectedDate = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
  final TextEditingController _dateController = new TextEditingController();
  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  String _email = "";
  String _password = "";
  FormType _form = FormType.register; // our default setting is to login, and we should switch to creating an account when the user chooses to

  _LoginPageState() {
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(

        context: context,
        
        initialDate: selectedDate,
        firstDate: DateTime(1910, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
      });
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty) {
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  _addDenuncia(String denunciaItem){
    if (denunciaItem.length > 0 ){
      
    }
  }
  

  void _dateListen(){

  }

  // Swap in between our two forms, registering and logging in
  void _formChange () async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
      } else {
        _form = FormType.register;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      
      appBar: _buildBar(context),
      body: 
      new SingleChildScrollView(child:
      new Container(
        padding: EdgeInsets.all(16.0),
        child: new Column(
          
          children: <Widget>[
            _buildTextFields(),
            _buildButtons(),
          ],
        ),
      ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    if (_form == FormType.login) {
      return new AppBar(
        title: new Text("Iniciar Sesion"),
        centerTitle: true,
      );
    } else {
      return new AppBar(
        title: new Text("Crear Cuenta"),
        centerTitle: true,
      );
    }
  } 

  Widget _buildTextFields() {
    if (_form == FormType.login) {
    return
    
    Form(
      key: _formKey,
       child: Container(
      child: new Column(
        children: <Widget>[
          new Container(
            child: new TextFormField(
              controller: _emailFilter,
              decoration: new InputDecoration(
                labelText: 'Email'
              ),
            ),
          ),
          new Container(
            child: new TextFormField(
              controller: _passwordFilter,
              decoration: new InputDecoration(
                labelText: 'Password'
              ),
              obscureText: true,
            ),
          ),
          Container(alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(_success == null 
          ? ''
          : (_success 
            ? 'Successfully Signed In!' 
            : 'Sign In failed'
          ),
          style:  TextStyle(color: Colors.red),
          ),)
        ],
      ),
       ),
    );
  } else { 
        return 
        Form(
          key: _formKey,
          child:
        Container(
        padding: EdgeInsets.all(8.0),
      child: new Column(
        children: <Widget>[
          new Container(
            child: new TextFormField(
              
              decoration: new InputDecoration(
                labelText: 'Nombre Completo'
              ),
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                  child: new TextField(
                    controller: _emailFilter,
                    decoration: new InputDecoration(labelText: 'Email'),
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  child: new TextField(
                    decoration: new InputDecoration(
                      labelText: 'Fecha de Nacimiento'
                    ),
                    enabled: false,
                    controller: TextEditingController(text: '$formattedDate'),
                    
                  )
                ),
                new Container(
                  child: RaisedButton(
                    onPressed: () => _selectDate(context),
                    child: Icon(Icons.calendar_today),
                    
                    ),
                  
                ),
                new Container( child: 
                 
                  new Column (
                    
                    children: 
                  
                    <Widget>[
                      new DropdownButton<String>(
                        
                        hint: Text('Sexo'),
                        value: _selectedGender ,
                        items: _genders.map((gender){
                          return new DropdownMenuItem<String>(
                            value: gender,
                            child: new Text(gender),
                          );
                          
                        }).toList(),
                        onChanged: (_) {
                          setState(() {
                           _selectedGender = (_); 
                          });
                        },
                        
                      )
                    ],
                  ),
                ),
                new Container(
                  child:
                  new Column(children: <Widget>[
                    new TextFormField(
                      decoration: new InputDecoration(labelText: 'Dirección'),
                    )
                  ],)
                )

              ],
            ),
            
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new TextFormField(
                  decoration: new InputDecoration(labelText: 'Teléfono'),
                )
              ],
            )),
          new Container(
            child: new TextFormField(
              controller: _passwordFilter,
              decoration: new InputDecoration(
                labelText: 'Password'
              ),
              obscureText: true,
            ),
          ),
          Container(alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(_success == null 
          ? ''
          : (_success 
            ? 'Successfully Registered!' 
            : 'Registration failed'
          ),
          style:  TextStyle(color: Colors.red),
          ),)
        ],
      ),
        ),
    );
  }
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(
          
          children: <Widget>[
            new RaisedButton(
              child: new Text('Login'),
              onPressed: () async {
                if (_formKey.currentState.validate() && _success == true){
                  // _signInWithEmailAndPassword();
                  Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),);
                }else{
                  setState(() {
                   _success = false; 
                  });
                }
              }
            ),
            new FlatButton(
              child: new Text('¿Usuario Nuevo? Crea una cuenta.'),
              onPressed: _formChange,
            ),
            new FlatButton(
              child: new Text('Recuperar Contraseña'),
              onPressed: _passwordReset,
            )
          ],
        ),
      );
    } else {
      return Container(
        child: new Column(
          children: <Widget>[
            new RaisedButton(
              child: new Text('Crear cuenta'),
              onPressed: _createAccountPressed,
            ),
            new FlatButton(
              child: new Text('¿Tienes cuenta? Iniciar Sesión.'),
              onPressed: _formChange,
            )
          ],
        ),
      );
    }
  }

  // These functions can self contain any user auth logic required, they all have access to _email and _password

  void _loginPressed () {
   /* if(_formKey.currentState.validate()){
      _signInWithEmailAndPassword();
    }*/

    print('The user wants to login with $_email and $_password');
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),);
  }

  void _createAccountPressed () {
    /*if (_formKey.currentState.validate()){
      _register();
      Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),);
    }*/
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),);
    print('The user wants to create an account with $_email and $_password');
      

  }

  void _passwordReset () {
    print("The user wants a password reset request sent to $_email");
  }

  @override
    void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailFilter.dispose();
    _passwordFilter.dispose();
    super.dispose();
  }

//   void _register() async {

//   try{
//   final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
//     email: _emailFilter.text,
//     password: _passwordFilter.text
//   )).user;
  
 

//     if (user != null){
//     setState(() {
//      _success = true;
//      _email = user.email;
//     });
//   }else{
//     _success = false;
//   }


//   } on PlatformException catch(error){
//           List<String> errors = error.toString().split(',');
//       print("Error: " + errors[1]);
//       _success = false;
//   }


// }


  // void _signInWithEmailAndPassword() async {

  //   try {
  //   final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
  //     email: _emailFilter.text,
  //     password: _passwordFilter.text,
  //   )).user;

   
  //   if (user != null) {
       
  //     setState(() {
  //       _success = true;
  //       _email = user.email;
  //     });

    
  //   } else {
  //     _success = false;
  //   }

  //   } on PlatformException catch (error){
  //        List<String> errors = error.toString().split(',');
  //     print("Error: " + errors[1]);
  //     _success = false;
  //   }

  // }
}




