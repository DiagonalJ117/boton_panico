import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Tab1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    Stack(alignment: Alignment.topCenter, children: <Widget>[
      Padding(
        child: Text("Mantener presionado por 3 segundos", 
        style: TextStyle(fontSize: 20),
      ), 
       padding: const EdgeInsets.only(top: 100),
      ),
    
    Container(
      child: Center(

        child: BotonPanico(),
      ),
    )
    ]
    );

  }
}

class BotonPanico extends StatefulWidget{
  @override
  BotonPanicoState createState() => BotonPanicoState();
}




class BotonPanicoState extends State<BotonPanico> with SingleTickerProviderStateMixin{
    AnimationController controller;
    bool _pressed = false;
    Position currentLocation;
    var geolocator = Geolocator();
    var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
Future <Position> _getLocation() async {

  try{
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    currentLocation = position;
    print(position.latitude);
    print(position.longitude);
    StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
    (Position position) {
        print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
    });
    print(positionStream);

  } catch (e) {
    debugPrint(e);
    currentLocation = null;
  }


}


  @override
  void initState(){
    super.initState();

    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));

    controller.addListener((){
      setState(() {
      if (controller.status == AnimationStatus.completed){
        _getLocation();
        _mostrarSnackbar();
      }


      });
    });

  }

    void _buttonPressed(){
        

      setState((){
        if (_pressed){
          _pressed = false;
        } else{
          _pressed = true;
        }
      });
      print("mensaje de debug: "+ _pressed.toString());
    }
    


    void _mostrarSnackbar(){
    final snackBar = SnackBar(
    content: Text('Alerta Enviada: Posicion: $currentLocation'),
    action: SnackBarAction(
      label: 'Deshacer',
      onPressed: () {
        Scaffold.of(context).removeCurrentSnackBar();
      },
    ),
  );

  void _mostrar3secs(){

  }

  Scaffold.of(context).showSnackBar(snackBar);
  }
  
      Widget build(BuildContext context){
        return GestureDetector(
          onTapDown: (_) { 
            controller.forward();

          },


          onTapUp: (_) {
            if (controller.status == AnimationStatus.forward){
              controller.reverse();
            }
            if(controller.status == AnimationStatus.completed){
              
              controller.reset();
            }
          }, 
          child: 
           Stack(
            alignment: Alignment.center,
            children: <Widget>[
              SizedBox(
                width:200,
                height:200,
                child:
              FloatingActionButton(
                
                child: Icon(Icons.warning),
              ),
              ),
             SizedBox(child:  CircularProgressIndicator(
                value: 1.0,
                strokeWidth: 5.0,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
              ),
              width: 200,
              height: 200
              ),
              SizedBox(
                
                width: 200,
                height: 200,
                child:               CircularProgressIndicator(
                strokeWidth: 5.0,
                value: controller.value,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
              ),
              
              ),


              
            ],
          ), 
          
          

          
          
          

        );
      }
}






