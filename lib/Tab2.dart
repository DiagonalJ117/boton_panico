import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:boton_panico/Tab3.dart';

void main() => runApp(new Tab2());

class Tab2 extends StatefulWidget {
  @override
  _Tab2State createState() => new _Tab2State();
}

class _Tab2State extends State<Tab2> {
  
  //final _user = User();

  int _radioValue1 = -1;
  int quizScore = 0;
  int _radioValue2 = -1;
  int _radioValue3 = -1;
  int _radioValue4 = -1;
  int _radioValue5 = -1;
  int _radioValue6 = -1;
  int _radioValue7 = -1;
  int _radioValue8 = -1;
  int _radioValue9 = -1;
  int _radioValue10 = -1;
  int _radioValue11 = -1;
  int _radioValue12 = -1;
  int _radioValue13 = -1;
  int _radioValue14 = -1;
  int _radioValue15 = -1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  void _handleRadioValueChange3(int value) {
    setState(() {
      _radioValue3 = value;
    });
  }

  void _handleRadioValueChange4(int value) {
    setState(() {
      _radioValue4 = value;
    });
  }

  void _handleRadioValueChange5(int value) {
    setState(() {
      _radioValue5 = value;
    });
  }

    void _handleRadioValueChange6(int value) {
    setState(() {
      _radioValue6 = value;
    });
  }
      void _handleRadioValueChange7(int value) {
    setState(() {
      _radioValue7 = value;
    });
  }
      void _handleRadioValueChange8(int value) {
    setState(() {
      _radioValue8 = value;
    });
  }
      void _handleRadioValueChange9(int value) {
    setState(() {
      _radioValue9 = value;
    });
  }
      void _handleRadioValueChange10(int value) {
    setState(() {
      _radioValue10 = value;
    });
  }
      void _handleRadioValueChange11(int value) {
    setState(() {
      _radioValue11 = value;
    });
  }
      void _handleRadioValueChange12(int value) {
    setState(() {
      _radioValue12 = value;
    });
  }
      void _handleRadioValueChange13(int value) {
    setState(() {
      _radioValue13 = value;
    });
  }
      void _handleRadioValueChange14(int value) {
    setState(() {
      _radioValue14 = value;
    });
  }
      void _handleRadioValueChange15(int value) {
    setState(() {
      _radioValue15 = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return 
    Scaffold(body:
    new SingleChildScrollView(
        child: new Container(
            padding: EdgeInsets.all(8.0),
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    'Selecciona las respustas que mas le identifiquen:',
                    style: new TextStyle(
                        fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  new Padding(
                    padding: new EdgeInsets.all(8.0),
                  ),
                  new Divider(height: 5.0, color: Colors.black),
                  new Padding(
                    padding: new EdgeInsets.all(8.0),
                  ),
                  new Text(
                    '¿Sientes que tu pareja constantemente te está controlando?',
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Radio(
                        value: 0,
                        groupValue: _radioValue1,
                        onChanged: _handleRadioValueChange1,
                      ),
                      new Text(
                        'Si',
                        style: new TextStyle(fontSize: 16.0),
                      ),
                      new Radio(
                        value: 1,
                        groupValue: _radioValue1,
                        onChanged: _handleRadioValueChange1,
                      ),
                      new Text(
                        'A veces',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                      new Radio(
                        value: 2,
                        groupValue: _radioValue1,
                        onChanged: _handleRadioValueChange1,
                      ),
                      new Text(
                        'Rara Vez',
                        style: new TextStyle(fontSize: 16.0),
                      ),
                      new Radio(
                        value: 3,
                        groupValue: _radioValue1,
                        onChanged: _handleRadioValueChange1,
                      ),
                      new Text(
                        'No',
                        style: new TextStyle(fontSize: 16.0),
                      ),
                    ],
                  ),
                  new Divider(
                    height: 5.0,
                    color: Colors.black,
                  ),
                  new Padding(
                    padding: new EdgeInsets.all(8.0),
                  ),
                  new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          '¿Te acusa de infidelidad o de que actúas en forma sospechosa?',
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Radio(
                              value: 0,
                              groupValue: _radioValue2,
                              onChanged: _handleRadioValueChange2,
                            ),
                            new Text(
                              'Si',
                              style: new TextStyle(fontSize: 16.0),
                            ),
                            new Radio(
                              value: 1,
                              groupValue: _radioValue2,
                              onChanged: _handleRadioValueChange2,
                            ),
                            new Text(
                              'A veces',
                              style: new TextStyle(fontSize: 16.0),
                            ),
                            new Radio(
                              value: 2,
                              groupValue: _radioValue2,
                              onChanged: _handleRadioValueChange2,
                            ),
                            new Text(
                              'Rara vez',
                              style: new TextStyle(fontSize: 16.0),
                            ),
                            new Radio(
                              value: 3,
                              groupValue: _radioValue2,
                              onChanged: _handleRadioValueChange2,
                            ),
                            new Text(
                              'No',
                              style: new TextStyle(fontSize: 16.0),
                            ),
                          ],
                        ),
                        
                        
                        new Divider(
                          height: 5.0,
                          color: Colors.black,
                        ),
                        new Padding(
                          padding: new EdgeInsets.all(8.0),
                        ),
                        new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Text(
                                '¿Has perdido contacto con amigas, familiares, compañeras/os de trabajo para evitar que tu pareja se moleste?',
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                ),
                              ),
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Radio(
                                    value: 0,
                                    groupValue: _radioValue3,
                                    onChanged: _handleRadioValueChange3,
                                  ),
                                  new Text(
                                    'Si',
                                    style: new TextStyle(fontSize: 16.0),
                                  ),
                                  new Radio(
                                    value: 1,
                                    groupValue: _radioValue3,
                                    onChanged: _handleRadioValueChange3,
                                  ),
                                  new Text(
                                    'A veces',
                                    style: new TextStyle(fontSize: 16.0),
                                  ),
                                  new Radio(
                                    value: 2,
                                    groupValue: _radioValue3,
                                    onChanged: _handleRadioValueChange3,
                                  ),
                                  new Text(
                                    'Rara vez',
                                    style: new TextStyle(fontSize: 16.0),
                                  ),
                                  new Radio(
                                    value: 3,
                                    groupValue: _radioValue3,
                                    onChanged: _handleRadioValueChange3,
                                  ),
                                  new Text(
                                    'No',
                                    style: new TextStyle(fontSize: 16.0),
                                  ),
                                ],
                              ),
                              new Divider(
                                height: 5.0,
                                color: Colors.black,
                              ),
                              new Padding(
                                padding: new EdgeInsets.all(8.0),
                              ),
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                    '¿Te critica y humilla, en público o en privado, sobre tu apariencia, tu forma de ser, el modo en que haces tus tareas hogareñas?',
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Radio(
                                        value: 0,
                                        groupValue: _radioValue4,
                                        onChanged: _handleRadioValueChange4,
                                      ),
                                      new Text(
                                        'Si',
                                        style: new TextStyle(fontSize: 16.0),
                                      ),
                                      new Radio(
                                        value: 1,
                                        groupValue: _radioValue4,
                                        onChanged: _handleRadioValueChange4,
                                      ),
                                      new Text(
                                        'A veces',
                                        style: new TextStyle(fontSize: 16.0),
                                      ),
                                      new Radio(
                                        value: 2,
                                        groupValue: _radioValue4,
                                        onChanged: _handleRadioValueChange4,
                                      ),
                                      new Text(
                                        'Rara vez',
                                        style: new TextStyle(fontSize: 16.0),
                                      ),
                                      new Radio(
                                        value: 3,
                                        groupValue: _radioValue4,
                                        onChanged: _handleRadioValueChange4,
                                      ),
                                      new Text(
                                        'No',
                                        style: new TextStyle(fontSize: 16.0),
                                      ),
                                    ],
                                  ),
                                  new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Controla estrictamente tus ingresos o el dinero que te entrega, originando discusiones?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue5,
                                            onChanged: _handleRadioValueChange5,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue5,
                                            onChanged: _handleRadioValueChange5,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue5,
                                            onChanged: _handleRadioValueChange5,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue5,
                                            onChanged: _handleRadioValueChange5,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                  new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        'Cuando quiere que cambies de comportamiento, ¿te presiona con el silencio, con la indiferencia o te priva de dinero?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue6,
                                            onChanged: _handleRadioValueChange6,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue6,
                                            onChanged: _handleRadioValueChange6,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue6,
                                            onChanged: _handleRadioValueChange6,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue6,
                                            onChanged: _handleRadioValueChange6,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Tiene tu pareja cambios bruscos de humor o se comporta distinto contigo en público, como si fuera otra persona?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue7,
                                            onChanged: _handleRadioValueChange7,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue7,
                                            onChanged: _handleRadioValueChange7,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue7,
                                            onChanged: _handleRadioValueChange7,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue7,
                                            onChanged: _handleRadioValueChange7,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Sientes que estás en permanente tensión y que, hagas lo que hagas, él se irrita o te culpabiliza?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue8,
                                            onChanged: _handleRadioValueChange8,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue8,
                                            onChanged: _handleRadioValueChange8,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue8,
                                            onChanged: _handleRadioValueChange8,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue8,
                                            onChanged: _handleRadioValueChange8,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Te ha golpeado con sus manos, con un objeto o te ha lanzado cosas cuando se enoja o discuten?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue9,
                                            onChanged: _handleRadioValueChange9,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue9,
                                            onChanged: _handleRadioValueChange9,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue9,
                                            onChanged: _handleRadioValueChange9,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue9,
                                            onChanged: _handleRadioValueChange9,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Te ha amenazado alguna vez con un objeto o arma, o con matarse él, a ti o a algún miembro de la familia?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue10,
                                            onChanged: _handleRadioValueChange10,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue10,
                                            onChanged: _handleRadioValueChange10,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue10,
                                            onChanged: _handleRadioValueChange10,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue10,
                                            onChanged: _handleRadioValueChange10,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Sientes que cedes a sus requerimientos sexuales por temor o te ha forzado a tener relaciones sexuales?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue11,
                                            onChanged: _handleRadioValueChange11,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue11,
                                            onChanged: _handleRadioValueChange11,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue11,
                                            onChanged: _handleRadioValueChange11,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue11,
                                            onChanged: _handleRadioValueChange11,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        'Después de un episodio violento, él se muestra cariñoso y atento, te regala cosas y te promete que nunca más volverá a golpearte o insultarte y que “todo cambiará”',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue12,
                                            onChanged: _handleRadioValueChange12,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue12,
                                            onChanged: _handleRadioValueChange12,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue12,
                                            onChanged: _handleRadioValueChange12,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue12,
                                            onChanged: _handleRadioValueChange12,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Has buscado o has recibido ayuda por lesiones que él te ha causado? (primeros auxilios, atención médica, psicológica o legal)',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue13,
                                            onChanged: _handleRadioValueChange13,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue13,
                                            onChanged: _handleRadioValueChange13,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue13,
                                            onChanged: _handleRadioValueChange13,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue13,
                                            onChanged: _handleRadioValueChange13,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Es violento con los hijos/as o con otras personas?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue14,
                                            onChanged: _handleRadioValueChange14,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue14,
                                            onChanged: _handleRadioValueChange14,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue14,
                                            onChanged: _handleRadioValueChange14,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue14,
                                            onChanged: _handleRadioValueChange14,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),new Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(
                                        '¿Ha sido necesario llamar a la policía o lo has intentado al sentir que tu vida y la de los tuyos han sido puestas en peligro por tu pareja?',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      new Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          new Radio(
                                            value: 0,
                                            groupValue: _radioValue15,
                                            onChanged: _handleRadioValueChange15,
                                          ),
                                          new Text(
                                            'Si',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 1,
                                            groupValue: _radioValue15,
                                            onChanged: _handleRadioValueChange15,
                                          ),
                                          new Text(
                                            'A veces',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 2,
                                            groupValue: _radioValue15,
                                            onChanged: _handleRadioValueChange15,
                                          ),
                                          new Text(
                                            'Rara vez',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                          new Radio(
                                            value: 3,
                                            groupValue: _radioValue15,
                                            onChanged: _handleRadioValueChange15,
                                          ),
                                          new Text(
                                            'No',
                                            style:
                                                new TextStyle(fontSize: 16.0),
                                          ),
                                        ],
                                      ),
                                      
                                      new Divider(
                                        height: 5.0,
                                        color: Colors.black,
                                      ),
                                      new Padding(
                                        padding: new EdgeInsets.all(8.0),
                                      ),
                                      new RaisedButton(
                                        onPressed: validateAnswers,
                                        child: new Text(
                                          'Checar Puntuación',
                                          style: new TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white),
                                        ),
                                        color: Theme.of(context).accentColor,
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    20.0)),
                                      ),
                                      new Padding(
                                        padding: EdgeInsets.all(4.0),
                                      ),
                                      new RaisedButton(
                                        onPressed: resetSelection,
                                        child: new Text(
                                          'Reiniciar',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                        color: Theme.of(context).accentColor,
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    20.0)),
                                      )
                                    ],
                                  ),
                                    ]
                                  )
                                ],
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                                  )
                                    ]
                              )
                            ])
                      ])
                ]
                )
                ]
            )
                )
    )
                );
  }

  void resetSelection() {
    _handleRadioValueChange1(-1);
    _handleRadioValueChange2(-1);
    _handleRadioValueChange3(-1);
    _handleRadioValueChange4(-1);
    _handleRadioValueChange5(-1);
    _handleRadioValueChange6(-1);
    _handleRadioValueChange7(-1);
    _handleRadioValueChange8(-1);
    _handleRadioValueChange9(-1);
    _handleRadioValueChange10(-1);
    _handleRadioValueChange11(-1);
    _handleRadioValueChange12(-1);
    _handleRadioValueChange13(-1);
    _handleRadioValueChange14(-1);
    _handleRadioValueChange15(-1);
    quizScore = 0;
  }

  void validateAnswers() {
    if (_radioValue1 == -1 &&
        _radioValue2 == -1 &&
        _radioValue3 == -1 &&
        _radioValue4 == -1 &&
        _radioValue5 == -1 &&
        _radioValue15 == -1
        ) {
      Fluttertoast.showToast(
          msg: 'Seleccione una respuesta', toastLength: Toast.LENGTH_SHORT);
    } else {
      quizScore = 0;
      addScores(_radioValue1);
      addScores(_radioValue2);
      addScores(_radioValue3);
      addScores(_radioValue4);
      addScores(_radioValue5);
      addScores(_radioValue6);
      addScores(_radioValue7);
      addScores(_radioValue8);
      addScores(_radioValue9);
      addScores(_radioValue10);
      addScores(_radioValue11);
      addScores(_radioValue12);
      addScores(_radioValue13);
      addScores(_radioValue14);
      addScores(_radioValue15);

      resultado(quizScore);

      Fluttertoast.showToast(
          msg: 'Tu puntuación final es de: $quizScore',
          toastLength: Toast.LENGTH_LONG);

        
    }
  }

  void addScores(radiovalue) {
    
    switch (radiovalue) {
      case 0:
        
           
        quizScore = quizScore + 3;
        break;
      case 1:
        
           
        quizScore = quizScore + 2;
        break;
      case 2:
       
        quizScore = quizScore + 1;
        break;
      case 3:
        break;
      default:
        break;
    }
  }

  void resultado(score){
    String resultString = '';
    String resultDescription = '';
    if (score <= 0){
      resultString = 'No existen problemas.';
      resultDescription = 'Si conoces a alguien que sufre algunas de estas situaciones, informalo al Instituto Sonorense de la Mujer para brindarle ayuda.';
    }
    if (score > 0 && score <= 11){
      resultString = 'Relacion abusiva';
      resultDescription = 'Existencia de problemas en los hogares, pero que se resuelven sin violencia física.';
    }
    if (score >= 12 && score <= 22){
      resultString = 'Indicios de Abuso Físico';
      resultDescription = 'La violencia en la relación está comenzando. Es una situación de ALERTA y un indicador de que la violencia puede aumentar en el futuro.';
    }
    if (score >= 23 && score <= 34){
      resultString = 'Abuso Severo';
      resultDescription = 'En este punto es importante solicitar ayuda institucional o personal y abandonar la casa temporalmente.';
    }
    if (score >= 35 && score <= 45){
      resultString = 'Situación de Violencia y Peligro de Muerte';
      resultDescription = 'Debes considerar en forma URGENTE e inmediata la posibilidad de dejar la relación en forma temporal y obtener apoyo externo, judicial y legal. El problema de violencia no se resuelve por sí mismo o conque ambos lo quieran. Tu vida puede llegar a estar en peligro en más de una ocasión o tu salud física o mental puede quedar permanentemente dañada.';
    }

    if (score > 12 && score <= 45){
      //enviar alerta de posible incidente a Seguridad Publica
    showDialog(context: context,
    builder: (BuildContext context){
      return AlertDialog(
        title: new Text("Resultado: $resultString"),
        content: new Text("$resultDescription"),
        actions: <Widget>[
          FlatButton(
            child: Text("Hacer Denuncia"),
            onPressed: (){
              
              Navigator.push(context, MaterialPageRoute(builder: (context) => Tab3()));
              
            }
          ),

          new FlatButton(
            child: new Text("Cerrar"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    },
    );
    } else {

    showDialog(context: context,
    builder: (BuildContext context){
      return AlertDialog(
        title: new Text("Resultado: $resultString"),
        content: new Text("$resultDescription"),
        actions: <Widget>[
          

          new FlatButton(
            child: new Text("Cerrar"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    },
    );
    }



    
  }
}
