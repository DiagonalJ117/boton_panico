
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:boton_panico/models/denuncia.dart';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:boton_panico/firebase_database_util.dart';

void main() => runApp(new Tab3());
// Create a Form widget.
class Tab3 extends StatefulWidget {
  @override
  _Tab3State createState() => new _Tab3State();
  
}

// Create a corresponding State class.
// This class holds data related to the form.
class _Tab3State extends State<Tab3> {

 

  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();

  }


  List<String> selectedRazones = [];
  Map<String, bool> razones ={
    'Bromas Hirientes': false,
    'Chantajes': false,
    'Mentir, Engañar': false,
    'Ignorar': false,
    'Celos': false,
    'Culpabilizar': false,
    'Descalificar': false,
    'Ridiculizar u ofender': false,
    'Humillar en publico': false,
    'Intimidar': false,
    'Controlar o Prohibir': false,
    'Destruir artículos personales': false,
    'Manosear': false,
    'Discriminación o Racismo': false,
    'Caricias agresivas': false,
    'Golpear (jugando)': false,
    'Pellizcar o arañar': false,
    'Empujar o jalonear': false,
    'Cachetear': false,
    'Patear': false,
    'Encerrar o aislar': false,
    'Amenazas con arma': false,
    'Amenazas de muerte': false,
    'Forzar una Relación Sexual': false,
    'Abuso Sexual': false,
    'Violación':false,
    'Mutilar': false,
    'Asesinar': false,
  };
  String _selectedIdentidad;
  List<String> _identidades = [
    'Pareja (Con quien no vivo)',
    'Ex Pareja', 
    'Esposo', 
    'Esposa',
    'Ex Esposo',
    'Ex Esposa',
    'Pareja con quien vivo',
    'Pareja con quien vivo y con hijos',
    ];
  int _radioValue1 = -1;
  int quizScore = 0;
  int _radioValue2 = -1;
  int _radioValue3 = -1;
  StreamSubscription<Event> _onDenunciaSentSubscription;
  Denuncia denuncia;
  static Position currentLocation;
  var geolocator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
  final Set<Marker> _markers = {};
   Future<Position> _getLocation() async {
      try{
        final GoogleMapController controller = await _controller.future;
        Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        currentLocation = position;
        print(position.latitude);
        print(position.longitude);
        controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
        _addMarker();
        
      } catch (e) {
        debugPrint(e);
        currentLocation = null;
      }

      
  }

  Future<Position> _getLiveLocation() async {
        final GoogleMapController controller = await _controller.future;
        StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
        (Position position) {
            print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
            controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
        });
        print(positionStream);
  }

  void _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
  }

  void _onRazonSelected(razon_id, bool selected){
    if (selected == true){
      setState(() {
       selectedRazones.add(razon_id); 
      });
    } else {
      setState(() {
       selectedRazones.remove(razon_id); 
      });
    }

  }

  void _addMarker() {
    setState(() {
     _markers.add(Marker(
       markerId:MarkerId(currentLocation.toString()),
       position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow( title: 'Posicion Actual'),
        icon: BitmapDescriptor.defaultMarker,
        
       )); 
    });
    print(currentLocation.toString());
  }

 

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),
    
  );

  static final CameraPosition _kPos = CameraPosition(
    
    target: LatLng(currentLocation.latitude, currentLocation.longitude),
    zoom: 17.0,
  );

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  void _handleRadioValueChange3(int value) {
    setState(() {
      _radioValue3 = value;
    });
  }

  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final _textEditingController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return 
    Scaffold(body:
    new SingleChildScrollView(child:
    new Container(
      padding: EdgeInsets.all(16.0),
      child:
    
    Form(
      
      key: _formKey,
      child: Column(
        
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: null,
            controller: _textEditingController,
            decoration: new InputDecoration(
              labelText: "Razon del Reporte"
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Enter some text';
              }
              return null;
            },
          ),
          new Padding(
              padding: new EdgeInsets.all(8.0),
          ),
          new Text('¿En estos momentos sufres violencia por parte de tu pareja?'),
          new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Radio(
                value: 1,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
              ),
              new Text(
                'Si'
              ),
              new Radio(
                value: 0,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
              ),
              new Text(
                'No'
              )
            ],
          ),
          Padding(
            padding: new EdgeInsets.all(8.0),
          ),
          Text(
            'Has realizado alguna denuncia?'
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                value: 1,
                groupValue: _radioValue2,
                onChanged: _handleRadioValueChange2,
              ),
              Text(
                'Si'
              ),
              Radio(
                value: 0,
                groupValue: _radioValue2,
                onChanged: _handleRadioValueChange2,
              ),
              Text(
                'No'
              )
            ],
          ),
          Padding(
            padding: new EdgeInsets.all(8.0),
          ),
          Text(
            'Elige las que aplican al caso'
          ),
          Column(
                children: razones.keys.map((String key){
                  return new CheckboxListTile(
                    title: new Text(key),
                    value: razones[key],
                    onChanged: (bool value){
                      setState(() {
                        razones[key] = value;
                      });
                    } ,
                  );
                }).toList(),
          ),
          Padding(
            padding: new EdgeInsets.all(8.0),
            ),
          Text('Identidad del agresor'),
          DropdownButton(
            hint: Text('Identidad'),
            value: _selectedIdentidad,
            items: _identidades.map((identidad){
              return new DropdownMenuItem<String>(
                value: identidad,
                child: new Text(identidad),
                );
            }).toList(),
            onChanged: (_){
              setState(() {
               _selectedIdentidad = (_); 
              });
            },
          ),
          Padding(
            padding: new EdgeInsets.all(8.0),
            ),
            SizedBox(
              
              height: 200.0,
              child: GoogleMap(
                markers: _markers,
              mapType: MapType.hybrid,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller){
                _controller.complete(controller);
              },
            ),),
            RaisedButton(
              onPressed: (){
                _getLocation();
              },
              child: Text("Ir a Posicion Actual"),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  // If the form is valid, display a Snackbar.
                  denunciaPressed(context);
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text('Enviado.')));
                }
              },
              child: Text('Enviar'),
            ),
          ),
        ],
      ),
    ),
    ),
    )
    );

  @override
  void addDenuncia(Denuncia denuncia){
    setState(() {
    
    });
  }


  }

void denunciaPressed( BuildContext context){
  AddDenunciaCallback _Tab3State;
  
  _Tab3State.addDenuncia(getData());
}

  Denuncia getData(){
    return new Denuncia("1", 'test user', _textEditingController.text, _radioValue1.toString(), _radioValue2.toString(), selectedRazones, _selectedIdentidad, currentLocation.toString());
  }
}

abstract class AddDenunciaCallback{
  void addDenuncia(Denuncia denuncia);
}
