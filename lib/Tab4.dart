import 'package:flutter/material.dart';

class Tab4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: new ContactList(kContacts),
      ),
    );
  }
}

const kContacts = const <Contact>[
      const Contact(
          fullName: 'ISM', email: 'ISM@example.com'),
      const Contact(fullName: '911', email: '911@example.com')
    ];

    class ContactList extends StatelessWidget {
      final List<Contact> _contacts;

      ContactList(this._contacts);

      @override
      Widget build(BuildContext context) {
        return new ListView.builder(
          padding: new EdgeInsets.symmetric(vertical: 8.0),
          itemBuilder: (context, index) {
            return new _ContactListItem(_contacts[index]);
          },
          itemCount: _contacts.length,
        );
      }
    }

    class _ContactListItem extends ListTile {
      _ContactListItem(Contact contact)
          : super(
                title: new Text(contact.fullName),
                subtitle: new Text(contact.email),
                leading: new CircleAvatar(child: new Text(contact.fullName[0])));
    }

    class Contact {
      final String fullName;
      final String email;

      const Contact({this.fullName, this.email});
    }