import 'package:flutter/material.dart';
import './Microcurso1.dart';
import './Microcurso2.dart';

class Tab5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          children: <Widget>[
            Expanded(
              flex:1,
              child:
                Card(
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Microcurso1()),);
                    },
                    child:
                     Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                    children:[

                    Container(
                      child: Center(
                      child: Text('Microcurso Empoderamiento de la Mujer', textAlign: TextAlign.center)
                      )
                    )
                    ]
                    )
                    )
                )
              
            ),
            Expanded(
              flex: 1,
              child:
                Card(
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    onTap: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context) => Microcurso2()),);
                    },
                    child: 
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                    children:[

                    Container(
                      child: Center(
                      child: Text('Microcurso Violencia de Género', textAlign: TextAlign.center)
                      )
                    )
                    ]
                    )
                    )
                )
              
            )
          ],
        )
      ),
    );
  }
}

