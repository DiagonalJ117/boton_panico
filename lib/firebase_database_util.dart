import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:boton_panico/models/denuncia.dart';
import 'package:boton_panico/models/alerta.dart';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseDatabaseUtil{
  DatabaseReference _denunciaRef;
  DatabaseReference _alertaRef;
  DatabaseReference _userRef;
  StreamSubscription<Event> _denunciaSubscription;
  StreamSubscription<Event> _alertaSubscription;
  FirebaseDatabase database = new FirebaseDatabase();

  DatabaseError error;

  static final FirebaseDatabaseUtil _instance = new FirebaseDatabaseUtil.internal();

  FirebaseDatabaseUtil.internal();

  factory FirebaseDatabaseUtil(){
    return _instance;
  }

  void initState(){
    _denunciaRef = FirebaseDatabase.instance.reference().child('denuncia');
    _alertaRef = FirebaseDatabase.instance.reference().child('alerta');
    _userRef = FirebaseDatabase.instance.reference().child('app_user');

    database.reference().child('denuncia').once().then((DataSnapshot snapshot){
       print('Connected to second database and read ${snapshot.value}');
    });
    database.setPersistenceEnabled(true);
    database.setPersistenceCacheSizeBytes(10000000);
    _denunciaRef.keepSynced(true);
    _alertaRef.keepSynced(true);

    _denunciaSubscription = _denunciaRef.onValue.listen((Event event){
      error=null;
    }, onError: (Object o){
      error = o;
    });
  }

  DatabaseError getError(){
    return error;
  }

  DatabaseReference getDenuncia(){
    return _denunciaRef;
  }

  DatabaseReference getAlerta(){
    return _alertaRef;
  }

  

   addDenuncia(Denuncia denuncia) async {
    final TransactionResult transactionResult = 
    await _denunciaRef.runTransaction((MutableData mutableData) async{
      mutableData.value = (mutableData.value ?? 0) + 1;
      return mutableData;
    });

    if (transactionResult.committed){
      _denunciaRef.push().set(<String, String>{
        "userId": "" + denuncia.userId,
        "razon": "" + denuncia.razon,
        "pregunta1": "" + denuncia.pregunta1,
        "pregunta2": "" + denuncia.pregunta2,
        "ofensas": "" + denuncia.ofensas.join(" "),
        "posicion": "" + denuncia.posicion,
        "idAgresor": "" + denuncia.idAgresor,
      }).then((_){
        print('Transaction committed');
      });
    }else{
      print('Transacion not committed');
      if(transactionResult.error != null){
        print(transactionResult.error.message);
      }
    }


  }

  setUserDetails(FirebaseUser user){
    
  }

  

  void dispose(){
    _alertaSubscription.cancel();
    _denunciaSubscription.cancel();
  }
  

}