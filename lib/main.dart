import 'package:boton_panico/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import './Tab1.dart';
import './Tab2.dart';
import './Tab3.dart';
import './Tab4.dart';
import './Tab5.dart';
import './Signup.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "APP VG",
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
      home: Signup()
     
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.userId, this.onSignedOut}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  //final BaseAuth auth;
  final VoidCallback onSignedOut;
  final String userId;
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _currentIndex = 0;
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();


  final List<Widget> _children = [
    Tab1(),
    Tab2(),
    Tab3(),
    Tab4(),
    Tab5(),
  ];


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("AppVG"),
      ),
      body: 
      _children[_currentIndex],
     
          bottomNavigationBar: BottomNavigationBar(
            
            onTap: onTabTapped,
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            items: [
              
              BottomNavigationBarItem(
                icon: new Icon(Icons.radio_button_checked),
                title: Text('Botón')
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.format_list_numbered),
                title: Text('Autoevaluación')
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.report),
                title: Text('Denunciar')
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.contacts),
                title: Text('Contactos')
              ),
              BottomNavigationBarItem(
                icon: new Icon(Icons.star),
                title: Text('Cursos')
              ),
            ],
          ),
     // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void onTabTapped(int index){
    setState(() {
     _currentIndex = index; 
    });
  }



}


