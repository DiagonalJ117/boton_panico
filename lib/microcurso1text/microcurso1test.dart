import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:boton_panico/Tab3.dart';

void main() => runApp(Microcurso1Test());

class Microcurso1Test extends StatefulWidget {
  @override
  _Microcurso1TestState createState() => _Microcurso1TestState();
}

class _Microcurso1TestState extends State<Microcurso1Test> {
  //final _user = User();

  int _radioValue1 = -1;
  int quizScore = 0;
  int _radioValue2 = -1;
  int _radioValue3 = -1;
  int _radioValue4 = -1;
  int _radioValue5 = -1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;
    });
  }

  void _handleRadioValueChange3(int value) {
    setState(() {
      _radioValue3 = value;
    });
  }

  void _handleRadioValueChange4(int value) {
    setState(() {
      _radioValue4 = value;
    });
  }

  void _handleRadioValueChange5(int value) {
    setState(() {
      _radioValue5 = value;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Test',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Text(
                        'Finalmente, después de haber estudiado con atención este microcurso sobre el empoderamiento de la mujer, te invitamos a responder a las siguientes preguntas.',
                        style: TextStyle(fontSize: 15.0),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Divider(height: 5.0, color: Colors.black),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Text(
                        '1. El empoderamiento implica:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RadioListTile(
                            title: Text('Fortalecer tu capacidad de liderazgo, organización y decisión'),
                            value: 0,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          RadioListTile(
                            title: Text('Abrir oportunidades para acceder a recursos y tener el control de los mismos.'),
                            value: 2,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          RadioListTile(
                            title: Text('Cambios en tu condición y posición como mujer.'),
                            value: 3,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          RadioListTile(
                            title: Text('Todos los anteriores'),
                            value: 1,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                        ],
                      ),
                      Divider(
                        height: 5.0,
                        color: Colors.black,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '2. Los factores que impiden el empoderamiento de la mujer son:',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                          RadioListTile(
                            title: Text('Falta de autoestima'),
                            value: 0,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                          RadioListTile(
                            title: Text('Los miedos'),
                            value: 2,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                          RadioListTile(
                            title: Text('La culpa'),
                            value: 3,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                          RadioListTile(
                            title: Text('Falta de asertividad'),
                            value: 5,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                                                    RadioListTile(
                            title: Text('Falta de recursos económicos'),
                            value: 4,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                                                    RadioListTile(
                            title: Text('Todos los anteriores'),
                            value: 1,
                            groupValue: _radioValue2,
                            onChanged: _handleRadioValueChange2,
                          ),
                              ],
                            ),
                            Divider(
                              height: 5.0,
                              color: Colors.black,
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '3. Uno de los aspectos que te brinda el empoderamiento es:',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                          RadioListTile(
                            title: Text('Mejor salud'),
                            value: 1,
                            groupValue: _radioValue3,
                            onChanged: _handleRadioValueChange3,
                          ),
                          RadioListTile(
                            title: Text('Fortalecer tu capacidad de liderazgo y organización'),
                            value: 2,
                            groupValue: _radioValue3,
                            onChanged: _handleRadioValueChange3,
                          ),
                          RadioListTile(
                            title: Text('Valorarte a ti misma'),
                            value: 3,
                            groupValue: _radioValue3,
                            onChanged: _handleRadioValueChange3,
                          ),
                                    ],
                                  ),
                                  Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          '4. La sororidad es:',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0,
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            RadioListTile(
                            title: Text('Conocer y aceptar tus sentimientos.'),
                            value: 2,
                            groupValue: _radioValue4,
                            onChanged: _handleRadioValueChange4,
                          ),
                          RadioListTile(
                            title: Text('El pacto social entre mujeres que tiene como objetivo el empoderamiento del género femenino.'),
                            value: 1,
                            groupValue: _radioValue4,
                            onChanged: _handleRadioValueChange4
                          ),
                          RadioListTile(
                            title: Text('Tener la posibilidad de participar en la toma de decisiones.'),
                            value: 3,
                            groupValue: _radioValue4,
                            onChanged: _handleRadioValueChange4,
                          ),
                                          ],
                                        ),
                                        Divider(
                                          height: 5.0,
                                          color: Colors.black,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(8.0),
                                        ),
                                        Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                '5. Lo que necesitas para empoderarte como mujer es:',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0,
                                                ),
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                                            RadioListTile(
                            title: Text('Conocerte a ti misma'),
                            value: 9,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Aceptarte a ti misma'),
                            value: 8,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Valorarte a ti misma'),
                            value: 7,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Cambiar lo que no te gusta'),
                            value: 6,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Respetar la diferencia'),
                            value: 5,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Exigir que te respeten'),
                            value: 4,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Tomar tus decisiones'),
                            value: 3,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                    RadioListTile(
                            title: Text('Conocer y aceptar tus sentimientos'),
                            value: 2,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                                             RadioListTile(
                            title: Text('Todas las anteriores'),
                            value: 1,
                            groupValue: _radioValue5,
                            onChanged: _handleRadioValueChange5
                          ),
                                                ],
                                              ),
                                              Divider(
                                                height: 5.0,
                                                color: Colors.black,
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(8.0),
                                              ),

                                              new RaisedButton(
                                        onPressed: validateAnswers,
                                        child: new Text(
                                          'Checar Puntuación',
                                          style: new TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white),
                                        ),
                                        color: Theme.of(context).accentColor,
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    20.0)),
                                      ),
                                      new Padding(
                                        padding: EdgeInsets.all(4.0),
                                      ),
                                      new RaisedButton(
                                        onPressed: resetSelection,
                                        child: new Text(
                                          'Reiniciar',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                        color: Theme.of(context).accentColor,
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    20.0)),
                                      )
                                            ])
                                      ])
                                ])
                          ])
                    ]))));
  }

  void resetSelection() {
    _handleRadioValueChange1(-1);
    _handleRadioValueChange2(-1);
    _handleRadioValueChange3(-1);
    _handleRadioValueChange4(-1);
    _handleRadioValueChange5(-1);
    quizScore = 0;
  }

  void validateAnswers() {
    if (_radioValue1 == -1 &&
        _radioValue2 == -1 &&
        _radioValue3 == -1 &&
        _radioValue4 == -1 &&
        _radioValue5 == -1) {
      Fluttertoast.showToast(
          msg: 'Seleccione una respuesta', toastLength: Toast.LENGTH_SHORT);
    } else {
      quizScore = 0;
      addScores(_radioValue1);
      addScores(_radioValue2);
      addScores(_radioValue3);
      addScores(_radioValue4);
      addScores(_radioValue5);

      resultado(quizScore);

      Fluttertoast.showToast(
          msg: 'Tu puntuación final es de: $quizScore',
          toastLength: Toast.LENGTH_LONG);
    }
  }

  void addScores(radiovalue) {
    switch (radiovalue) {
      case 0:
        break;
      case 1:
        quizScore = quizScore + 1;
        break;
      default:
        break;
    }
  }

  void resultado(score) {
    String resultString = '';
    String resultDescription = '';

    if (score >= 12 && score <= 45) {
      //enviar alerta de posible incidente a Seguridad Publica
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado: "),
            content: Text("$quizScore / 5"),
            actions: <Widget>[
              FlatButton(
                  child: Text("Hacer Denuncia"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Tab3()));
                  }),
              FlatButton(
                child: Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado: $resultString"),
            content: Text("$quizScore / 5"),
            actions: <Widget>[
              FlatButton(
                child: Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }
}
