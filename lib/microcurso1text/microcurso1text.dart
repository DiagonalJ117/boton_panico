import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

const String _text11 = """

El empoderamiento es un proceso personal y progresivo en el cual la mujer reconoce su derecho a ser y sentirse capaz, a reconocer su autoridad, a legitimarse, experimentar su seguridad, desarrollar su toma de decisiones y ejercerla.

Mediante este proceso, como mujer, transitas de cualquier situación de opresión, desigualdad, discriminación, explotación o exclusión y adquieres conciencia, autodeterminación y autonomía.

El empoderamiento implica:

  •	Fortalecer tus capacidades.
  •	Fortalecer tu capacidad de liderazgo y organización
  •	Fortalecer tu capacidad de decisión
  •	Abrir oportunidades para acceder a recursos y tener el control de los mismos.
  •	Cambios en tu condición y posición como mujer.

""";

const String _text3 = """

•	Poder económico: beneficios materiales, los ingresos, las tierras, las herramientas o las tecnologías.

•	Mejor salud

•	Ganancia de tiempo

•	Acceso a ciertos servicios como el crédito, la información y la formación, los centros de salud, el mercado, entre otros.


""";

const String _text31 = """

Pone de manifiesto la importancia de la aplicación de los conocimientos o la capacidad de traducir los conocimientos en acciones o en recursos.

•	Conocimientos o competencias prácticas e intelectuales reforzadas que te permiten gozar de manera óptima de las oportunidades que, como mujer, se te presentan.

•	Se trata de la gestión de personas (liderazgo), de técnicas o procedimientos, de las formaciones (alfabetización, etc.) y del desarrollo de las capacidades de análisis crítico del pensamiento y del razonamiento.


""";

const String _text32 = """
•	Se trata del poder interno, la fuerza psicológica o el poder espiritual que posees: valores, miedos, la confianza y la imagen de ti misma.

•	La capacidad y la voluntad de hacer por ti misma elecciones sobre tu futuro.
 
•	Tomar conciencia de tu propio proyecto de vida y de los retos a los que se enfrenta tu comunidad.


""";


const String _text33 = """

Tener la posibilidad de tomar decisiones, de asumir responsabilidades, de ser libre en tus actos y de utilizar recursos propios (tener, saber, querer). 
La toma de decisiones engloba varios aspectos:

•	Tener la posibilidad de tomar decisiones por sí misma.

•	Tener la posibilidad de participar en la toma de decisiones.

•	Tener la posibilidad de influir en la toma de decisiones y controlar a aquellos o aquellas que tomen las decisiones en tu nombre.

•	Tener la posibilidad de tomar decisiones por los otros, de mandar (en el sentido de que en ciertas situaciones siempre hay una persona debe tomar una decisión por todos).


""";


const String _text4 = """
La sororidad es el pacto social entre mujeres que tiene como objetivo el empoderamiento del género femenino.

Así, cuando  una amiga o conocida te cuente una situación de violencia, acoso  o agresión, no la critiques ni la juzgues. Ayúdala proporcionándole información sobre centros especializados, según sea el caso.

No asumas que una mujer se encuentra en un puesto  de poder porque se acostó con el jefe. Y, sobre todo, no inicies tú el rumor.

""";

const String _text5 = """

El empoderamiento designa en la persona o en una comunidad, la capacidad de actuar de forma autónoma, así como los medios necesarios y el proceso para lograr esta capacidad de actuar, de tomar tus propias decisiones en lo que tú elijas.
El enfoque del empoderamiento se hace, pues, a dos niveles:

1.	En relación con la capacidad de cambio personal

2.	En relación con el cambio político y social

Finalmente, para empoderarte como mujer te sugerimos:

•	Conocerte a ti misma.

•	Aceptarte a ti misma.

•	Valorarte a ti misma.

•	Cambiar lo que no te gusta.

•	Respetar la diferencia.

•	Exigir que te respeten

•	Tomar tus decisiones

•	Conocer y aceptar tus sentimientos


""";

class Microcurso1Intro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('Microcurso Empoderamiento de la Mujer',
                style: TextStyle(fontSize: 15.0))
          ]),
          SizedBox(height: 20),
          RichText(
              text: TextSpan(
            text: '',
            style: TextStyle(fontSize: 15, color: Colors.black),
            children: <TextSpan>[
              TextSpan(
                text:
                    'Este curso breve tiene como objetivo proporcionarte información sobre el poder vital que tienes como ser humano y fortalezcas tu toma de decisiones y autonomía. ',
              ),
            ],
          ))
        ],
      ),
    );
  }
}

class Microcurso1Cap1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1. Empoderamiento',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          RichText(
              text: TextSpan(
            text: '',
            style: TextStyle(fontSize: 15, color: Colors.black),
            children: <TextSpan>[
              TextSpan(text: 'El'),
              TextSpan(
                  text: ' empoderamiento de la mujer',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(
                  text:
                      ' “se relaciona con el poder entendido, no como dominación sobre los demás, sino como capacidad de las mujeres de aumentar su auto confianza e influir en los cambios”. '),
            ],
          ))
        ],
      ),
    );
  }
}

class Microcurso1Cap11 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.1 Proceso de Empoderamiento',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text11))
        ],
      ),
    );
  }
}

class Microcurso1Cap2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2. Factores que impiden el empoderamiento',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
        ],
      ),
    );
  }
}

class Microcurso1Cap3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('3. Cuatro aspectos que te brinda el empoderamiento',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text3))
        ],
      ),
    );
  }
}

class Microcurso1Cap31 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('3.1. Saber y saber hacer',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text31))
        ],
      ),
    );
  }
}

class Microcurso1Cap32 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('3.2. Poder interior',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text32))
        ],
      ),
    );
  }
}

class Microcurso1Cap33 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('3.3. Poder interior y poder con',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text33))
        ],
      ),
    );
  }
}


class Microcurso1Cap4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('4. Sororidad',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text4))
        ],
      ),
    );
  }
}


class Microcurso1Cap5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('5. Conclusiones',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text5))
        ],
      ),
    );
  }
}
