import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:boton_panico/Tab3.dart';

void main() => runApp(Microcurso2Test());

class Microcurso2Test extends StatefulWidget {
  @override
  _Microcurso2TestState createState() => _Microcurso2TestState();
}

class _Microcurso2TestState extends State<Microcurso2Test> {
  //final _user = User();

  int _radioValue1 = -1;
  int quizScore = 0;
  int _radioValue2 = -1;
  int _radioValue3 = -1;
  int _radioValue4 = -1;
  int _radioValue5 = -1;
  int _radioValue6 = -1;
  int _radioValue7 = -1;
  int _radioValue8 = -1;
  int _radioValue9 = -1;
  int _radioValue10 = -1;
  int _radioValue11 = -1;
  int _radioValue12 = -1;
  int _radioValue13 = -1;

  bool _vis1 = false;
  bool _vis2 = false;
  bool _vis3 = false;
  bool _vis4 = false;
  bool _vis5 = false;
  bool _vis6 = false;
  bool _vis7 = false;
  bool _vis8 = false;
  bool _vis9 = false;
  bool _vis10 = false;
  bool _vis11 = false;
  bool _vis12 = false;
  bool _vis13 = false;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      if (_radioValue1 == 0) {
        _vis1 = true;
      } else {
        _vis1 = false;
      }
    });
  }

  void _handleRadioValueChange2(int value) {
    setState(() {
      _radioValue2 = value;

      if (_radioValue2 == 0) {
        _vis2 = true;
      } else {
        _vis2 = false;
      }
    });
  }

  void _handleRadioValueChange3(int value) {
    setState(() {
      _radioValue3 = value;

      if (_radioValue3 == 0) {
        _vis3 = true;
      } else {
        _vis3 = false;
      }
    });
  }

  void _handleRadioValueChange4(int value) {
    setState(() {
      _radioValue4 = value;
      if (_radioValue4 == 0) {
        _vis4 = true;
      } else {
        _vis4 = false;
      }
    });
  }

  void _handleRadioValueChange5(int value) {
    setState(() {
      _radioValue5 = value;

      if (_radioValue5 == 0) {
        _vis5 = true;
      } else {
        _vis5 = false;
      }
    });
  }

  void _handleRadioValueChange6(int value) {
    setState(() {
      _radioValue6 = value;
      if (_radioValue6 == 0) {
        _vis6 = true;
      } else {
        _vis6 = false;
      }
    });
  }

  void _handleRadioValueChange7(int value) {
    setState(() {
      _radioValue7 = value;
      if (_radioValue7 == 0) {
        _vis7 = true;
      } else {
        _vis7 = false;
      }
    });
  }

  void _handleRadioValueChange8(int value) {
    setState(() {
      _radioValue8 = value;
      if (_radioValue8 == 0) {
        _vis8 = true;
      } else {
        _vis8 = false;
      }
    });
  }

  void _handleRadioValueChange9(int value) {
    setState(() {
      _radioValue9 = value;
      if (_radioValue9 == 0) {
        _vis9 = true;
      } else {
        _vis9 = false;
      }
    });
  }

  void _handleRadioValueChange10(int value) {
    setState(() {
      _radioValue10 = value;
      if (_radioValue10 == 0) {
        _vis10 = true;
      } else {
        _vis10 = false;
      }
    });
  }

  void _handleRadioValueChange11(int value) {
    setState(() {
      _radioValue11 = value;

      if (_radioValue11 == 0) {
        _vis11 = true;
      } else {
        _vis11 = false;
      }
    });
  }

  void _handleRadioValueChange12(int value) {
    setState(() {
      _radioValue12 = value;

      if (_radioValue12 == 0) {
        _vis12 = true;
      } else {
        _vis12 = false;
      }
    });
  }

  void _handleRadioValueChange13(int value) {
    setState(() {
      _radioValue13 = value;

      if (_radioValue13 == 0) {
        _vis13 = true;
      } else {
        _vis13 = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Test',
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Text(
                        'Finalmente, después de haber estudiado con atención este microcurso sobre violencia de género, te invitamos a responder al siguiente test.',
                        style: TextStyle(fontSize: 15.0),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Divider(height: 5.0, color: Colors.black),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Text(
                        '“Las mujeres buscan hombres que ejercen violencia.” ',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RadioListTile(
                            title: Text('Verdadero'),
                            value: 0,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          RadioListTile(
                            title: Text('Falso'),
                            value: 1,
                            groupValue: _radioValue1,
                            onChanged: _handleRadioValueChange1,
                          ),
                          Visibility(
                              visible: _vis1,
                              child: Text(
                                'Incorrecto. \nRealidad: Estas mujeres suelen poseer convicciones rígidas en relación al papel de la mujer y su subordinación al hombre; piensan que con paciencia y tolerancia ayudarán a mejorarlo, al no lograr el cambio, callan por vergüenza.',
                                style: TextStyle(color: Colors.redAccent),
                              )),
                        ],
                      ),
                      Divider(
                        height: 5.0,
                        color: Colors.black,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '“A la mujer le agrada la violencia, si no, abandonaría a su pareja.”',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RadioListTile(
                                  title: Text('Verdadero'),
                                  value: 0,
                                  groupValue: _radioValue2,
                                  onChanged: _handleRadioValueChange2,
                                ),
                                RadioListTile(
                                  title: Text('Falso'),
                                  value: 1,
                                  groupValue: _radioValue2,
                                  onChanged: _handleRadioValueChange2,
                                ),
                                Visibility(
                                    visible: _vis2,
                                    child: Text(
                                      'Incorrecto. \nRealidad: A ninguna mujer le agrada vivir en violencia, por otros motivos no abandonan a sus parejas, ya sean emocionales, sociales o económicos. ',
                                      style: TextStyle(color: Colors.redAccent),
                                    )),
                              ],
                            ),
                            Divider(
                              height: 5.0,
                              color: Colors.black,
                            ),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '“¿Por qué aguantó tanto tiempo y ahora se queja?”',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      RadioListTile(
                                        title: Text('Verdadero'),
                                        value: 0,
                                        groupValue: _radioValue3,
                                        onChanged: _handleRadioValueChange3,
                                      ),
                                      RadioListTile(
                                        title: Text('Falso'),
                                        value: 1,
                                        groupValue: _radioValue3,
                                        onChanged: _handleRadioValueChange3,
                                      ),
                                      Visibility(
                                          visible: _vis3,
                                          child: Text(
                                          'Incorrecto. \nRealidad: Las mujeres denuncian en promedio después de siete años de sufrir violencia. No es fácil denunciar, están las creencias mencionadas.',
                                            style: TextStyle(
                                                color: Colors.redAccent),
                                          )),
                                    ],
                                  ),
                                  Divider(
                                    height: 5.0,
                                    color: Colors.black,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          '“La violencia es un problema de las clases más bajas.”',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0,
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            RadioListTile(
                                              title: Text('Verdadero'),
                                              value: 0,
                                              groupValue: _radioValue4,
                                              onChanged:
                                                  _handleRadioValueChange4,
                                            ),
                                            RadioListTile(
                                                title: Text('Falso'),
                                                value: 1,
                                                groupValue: _radioValue4,
                                                onChanged:
                                                    _handleRadioValueChange4),
                                            Visibility(
                                                visible: _vis4,
                                                child: Text(
                                                  'Incorrecto. \nRealidad: En la práctica, se conoce que esta realidad traspasa las clases sociales, por lo que no es atribuible a factores derivados de la estructura socioeconómica. ',
                                                  style: TextStyle(
                                                      color: Colors.redAccent),
                                                )),
                                          ],
                                        ),
                                        Divider(
                                          height: 5.0,
                                          color: Colors.black,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(8.0),
                                        ),
                                        Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                '“Quien te quiere te pega.” ',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0,
                                                ),
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  RadioListTile(
                                                      title: Text('Verdadero'),
                                                      value: 0,
                                                      groupValue: _radioValue5,
                                                      onChanged:
                                                          _handleRadioValueChange5),
                                                  RadioListTile(
                                                      title: Text('Falso'),
                                                      value: 1,
                                                      groupValue: _radioValue5,
                                                      onChanged:
                                                          _handleRadioValueChange5),
                                                  Visibility(
                                                      visible: _vis5,
                                                      child: Text(
                                                        'Incorrecto. \nRealidad: Muchas veces estos patrones se aprenden en el hogar, en dónde la violencia es la forma de interacción conocida. Además, se vincula a los celos que sorprende como explicación que se dan las propias mujeres, al encontrar en ellas una vivencia culposa de base.  ',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .redAccent),
                                                      )),
                                                ],
                                              ),
                                              Divider(
                                                height: 5.0,
                                                color: Colors.black,
                                              ),
                                              Padding(
                                                padding: EdgeInsets.all(8.0),
                                              ),
                                              Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Text(
                                                      '“Las personas educadas y cultas no son violentas.” ',
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0,
                                                      ),
                                                    ),
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        RadioListTile(
                                                            title: Text(
                                                                'Verdadero'),
                                                            value: 0,
                                                            groupValue:
                                                                _radioValue6,
                                                            onChanged:
                                                                _handleRadioValueChange6),
                                                        RadioListTile(
                                                            title:
                                                                Text('Falso'),
                                                            value: 1,
                                                            groupValue:
                                                                _radioValue6,
                                                            onChanged:
                                                                _handleRadioValueChange6),
                                                        Visibility(
                                                            visible: _vis6,
                                                            child: Text(
                                                              'Incorrecto. \nRealidad: Cualquier mujer puede ser víctima de violencia como consecuencia de la cultura de dominación y control de la mujer, y esta no dependerá de la educación formal que se tenga, sino más bien de la educación familiar y social. ',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .redAccent),
                                                            )),
                                                      ],
                                                    ),
                                                    Divider(
                                                      height: 5.0,
                                                      color: Colors.black,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(8.0),
                                                    ),
                                                    Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Text(
                                                            '“Se trata de casos aislados”',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 18.0,
                                                            ),
                                                          ),
                                                          Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              RadioListTile(
                                                                  title: Text(
                                                                      'Verdadero'),
                                                                  value: 0,
                                                                  groupValue:
                                                                      _radioValue7,
                                                                  onChanged:
                                                                      _handleRadioValueChange7),
                                                              RadioListTile(
                                                                  title: Text(
                                                                      'Falso'),
                                                                  value: 1,
                                                                  groupValue:
                                                                      _radioValue7,
                                                                  onChanged:
                                                                      _handleRadioValueChange7),
                                                              Visibility(
                                                                  visible:
                                                                      _vis7,
                                                                  child: Text(
                                                                    'Incorrecto. \nRealidad: Las estadísticas de prevalencia indican que a nivel mundial una de cada diez mujeres es o ha sido agredida por su pareja. Este mito es un intento por negar la verdad y no hacerse cargo, al no considerarlo un problema social. ',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .redAccent),
                                                                  )),
                                                            ],
                                                          ),
                                                          Divider(
                                                            height: 5.0,
                                                            color: Colors.black,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    8.0),
                                                          ),
                                                          Column(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  '“La violencia en la familia es habitualmente ocasionada por el alcohol y las drogas.” ',
                                                                  style:
                                                                      TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        18.0,
                                                                  ),
                                                                ),
                                                                Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: <
                                                                      Widget>[
                                                                    RadioListTile(
                                                                        title: Text(
                                                                            'Verdadero'),
                                                                        value:
                                                                            0,
                                                                        groupValue:
                                                                            _radioValue8,
                                                                        onChanged:
                                                                            _handleRadioValueChange8),
                                                                    RadioListTile(
                                                                        title: Text(
                                                                            'Falso'),
                                                                        value:
                                                                            1,
                                                                        groupValue:
                                                                            _radioValue8,
                                                                        onChanged:
                                                                            _handleRadioValueChange8),
                                                                    Visibility(
                                                                        visible:
                                                                            _vis8,
                                                                        child:
                                                                            Text(
                                                                          'Incorrecto. \nRealidad: Existen alcohólicos que no utilizan la violencia, como hay personas que son violentas y no ingieren alcohol. Comúnmente el agresor que ha ingerido alcohol no violenta a cualquier persona sino a su mujer, actuando en ello los factores sociales mencionados. ',
                                                                          style:
                                                                              TextStyle(color: Colors.redAccent),
                                                                        )),
                                                                  ],
                                                                ),
                                                                Divider(
                                                                  height: 5.0,
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                                Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              8.0),
                                                                ),
                                                                Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        '“La violencia en la familia es un asunto privado y se debe arreglar al interior del hogar.”',
                                                                        style:
                                                                            TextStyle(
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                          fontSize:
                                                                              18.0,
                                                                        ),
                                                                      ),
                                                                      Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.center,
                                                                        children: <
                                                                            Widget>[
                                                                          RadioListTile(
                                                                              title: Text('Verdadero'),
                                                                              value: 0,
                                                                              groupValue: _radioValue9,
                                                                              onChanged: _handleRadioValueChange9),
                                                                          RadioListTile(
                                                                              title: Text('Falso'),
                                                                              value: 1,
                                                                              groupValue: _radioValue9,
                                                                              onChanged: _handleRadioValueChange9),
                                                                          Visibility(
                                                                              visible: _vis9,
                                                                              child: Text(
                                                                                'Incorrecto. \nRealidad: Vivir y desarrollarse en un ambiente libre de violencia es un derecho esencial de todo ser humano. Asimismo es deber del Estado resguardar este derecho. ',
                                                                                style: TextStyle(color: Colors.redAccent),
                                                                              )),
                                                                        ],
                                                                      ),
                                                                      Divider(
                                                                        height:
                                                                            5.0,
                                                                        color: Colors
                                                                            .black,
                                                                      ),
                                                                      Padding(
                                                                        padding:
                                                                            EdgeInsets.all(8.0),
                                                                      ),
                                                                      Column(
                                                                          mainAxisAlignment: MainAxisAlignment
                                                                              .center,
                                                                          children: <
                                                                              Widget>[
                                                                            Text(
                                                                              '“Los agresores son enfermos mentales.”',
                                                                              style: TextStyle(
                                                                                fontWeight: FontWeight.bold,
                                                                                fontSize: 18.0,
                                                                              ),
                                                                            ),
                                                                            Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                RadioListTile(title: Text('Verdadero'), value: 0, groupValue: _radioValue10, onChanged: _handleRadioValueChange10),
                                                                                RadioListTile(title: Text('Falso'), value: 1, groupValue: _radioValue10, onChanged: _handleRadioValueChange10),
                                                                                Visibility(
                                                                                    visible: _vis10,
                                                                                    child: Text(
                                                                                      'Incorrecto. \nRealidad: Menos del 10% de las situaciones de violencia están ocasionados por trastornos psiquiátricos.',
                                                                                      style: TextStyle(color: Colors.redAccent),
                                                                                    )),
                                                                              ],
                                                                            ),
                                                                            Divider(
                                                                              height: 5.0,
                                                                              color: Colors.black,
                                                                            ),
                                                                            Padding(
                                                                              padding: EdgeInsets.all(8.0),
                                                                            ),
                                                                            Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                                                              Text(
                                                                                '“¿Cómo se las va a arreglar sola?”',
                                                                                style: TextStyle(
                                                                                  fontWeight: FontWeight.bold,
                                                                                  fontSize: 18.0,
                                                                                ),
                                                                              ),
                                                                              Column(
                                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                                children: <Widget>[
                                                                                  RadioListTile(title: Text('Verdadero'), value: 0, groupValue: _radioValue11, onChanged: _handleRadioValueChange11),
                                                                                  RadioListTile(title: Text('Falso'), value: 1, groupValue: _radioValue11, onChanged: _handleRadioValueChange11),
                                                                                  Visibility(
                                                                                      visible: _vis11,
                                                                                      child: Text(
                                                                                        'Incorrecto. \nRealidad: En la actualidad las mujeres pueden desarrollarse laboralmente a la par que los hombres. El que la mujer no busque empleo muchas veces se debe a que la pareja se lo impide, como otra forma de ejercer poder sobre ella y generar dependencia. Esto se vuelve un círculo vicioso en dónde ella misma llega a creer lo que él le dice, en relación a que no se podrá mantener por sí misma.',
                                                                                        style: TextStyle(color: Colors.redAccent),
                                                                                      )),
                                                                                ],
                                                                              ),
                                                                              Divider(
                                                                                height: 5.0,
                                                                                color: Colors.black,
                                                                              ),
                                                                              Padding(
                                                                                padding: EdgeInsets.all(8.0),
                                                                              ),
                                                                              Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                                                                Text(
                                                                                  '“La violencia es algo innato, que pertenece a la esencia del ser humano“ ',
                                                                                  style: TextStyle(
                                                                                    fontWeight: FontWeight.bold,
                                                                                    fontSize: 18.0,
                                                                                  ),
                                                                                ),
                                                                                Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                  children: <Widget>[
                                                                                    RadioListTile(title: Text('Verdadero'), value: 0, groupValue: _radioValue12, onChanged: _handleRadioValueChange12),
                                                                                    RadioListTile(title: Text('Falso'), value: 1, groupValue: _radioValue12, onChanged: _handleRadioValueChange12),
                                                                                    Visibility(
                                                                                        visible: _vis12,
                                                                                        child: Text(
                                                                                          'Incorrecto. \nRealidad: Actualmente se considera que la violencia es una conducta aprendida a través de patrones familiares y sociales, lo cual es de particular importancia en lo referido a la violencia del hombre contra la mujer, ya que este hombre, por lo general, no es un hombre violento en el resto de sus relaciones. ',
                                                                                          style: TextStyle(color: Colors.redAccent),
                                                                                        )),
                                                                                  ],
                                                                                ),
                                                                                Divider(
                                                                                  height: 5.0,
                                                                                  color: Colors.black,
                                                                                ),
                                                                                Padding(
                                                                                  padding: EdgeInsets.all(8.0),
                                                                                ),
                                                                                Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                                                                  Text(
                                                                                    '“La mujer es golpeada porque no cumple con sus obligaciones.”',
                                                                                    style: TextStyle(
                                                                                      fontWeight: FontWeight.bold,
                                                                                      fontSize: 18.0,
                                                                                    ),
                                                                                  ),
                                                                                  Column(
                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                    children: <Widget>[
                                                                                      RadioListTile(title: Text('Verdadero'), value: 0, groupValue: _radioValue13, onChanged: _handleRadioValueChange13),
                                                                                      RadioListTile(title: Text('Falso'), value: 1, groupValue: _radioValue13, onChanged: _handleRadioValueChange13),
                                                                                      Visibility(
                                                                                          visible: _vis13,
                                                                                          child: Text(
                                                                                            'Incorrecto. \nRealidad: Esta creencia se sustenta en estereotipos sexuales rígidos, basados en la creencia que la mujer debiera dedicarse de forma absoluta al hogar y al marido, y como forma de justificar la violencia. ',
                                                                                            style: TextStyle(color: Colors.redAccent),
                                                                                          )),
                                                                                    ],
                                                                                  ),
                                                                                  Divider(
                                                                                    height: 5.0,
                                                                                    color: Colors.black,
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: EdgeInsets.all(8.0),
                                                                                  ),
                                                                                  new RaisedButton(
                                                                                    onPressed: validateAnswers,
                                                                                    child: new Text(
                                                                                      'Checar Puntuación',
                                                                                      style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.normal, color: Colors.white),
                                                                                    ),
                                                                                    color: Theme.of(context).accentColor,
                                                                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                                                                                  ),
                                                                                  new Padding(
                                                                                    padding: EdgeInsets.all(4.0),
                                                                                  ),
                                                                                  new RaisedButton(
                                                                                    onPressed: resetSelection,
                                                                                    child: new Text(
                                                                                      'Reiniciar',
                                                                                      style: new TextStyle(fontWeight: FontWeight.normal, fontSize: 16.0, color: Colors.white),
                                                                                    ),
                                                                                    color: Theme.of(context).accentColor,
                                                                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                                                                                  )
                                                                                ])
                                                                              ])
                                                                            ])
                                                                          ])
                                                                    ])
                                                              ])
                                                        ])
                                                  ])
                                            ])
                                      ])
                                ])
                          ])
                    ]))));
  }

  void resetSelection() {
    _handleRadioValueChange1(-1);
    _handleRadioValueChange2(-1);
    _handleRadioValueChange3(-1);
    _handleRadioValueChange4(-1);
    _handleRadioValueChange5(-1);
    _handleRadioValueChange6(-1);
    _handleRadioValueChange7(-1);
    _handleRadioValueChange8(-1);
    _handleRadioValueChange9(-1);
    _handleRadioValueChange10(-1);
    _handleRadioValueChange11(-1);
    _handleRadioValueChange12(-1);
    _handleRadioValueChange13(-1);
    quizScore = 0;
  }

  void validateAnswers() {
    if (_radioValue1 == -1 &&
        _radioValue2 == -1 &&
        _radioValue3 == -1 &&
        _radioValue4 == -1 &&
        _radioValue5 == -1) {
      Fluttertoast.showToast(
          msg: 'Seleccione una respuesta', toastLength: Toast.LENGTH_SHORT);
    } else {
      quizScore = 0;
      addScores(_radioValue1);
      addScores(_radioValue2);
      addScores(_radioValue3);
      addScores(_radioValue4);
      addScores(_radioValue5);
      addScores(_radioValue6);
      addScores(_radioValue7);
      addScores(_radioValue8);
      addScores(_radioValue9);
      addScores(_radioValue10);
      addScores(_radioValue11);
      addScores(_radioValue12);
      addScores(_radioValue13);

      resultado(quizScore);

      Fluttertoast.showToast(
          msg: 'Tu puntuación final es de: $quizScore',
          toastLength: Toast.LENGTH_LONG);
    }
  }

  void addScores(radiovalue) {
    switch (radiovalue) {
      case 0:
        break;
      case 1:
        quizScore = quizScore + 1;
        break;
      default:
        break;
    }
  }

  void resultado(score) {
    String resultString = '';
    String resultDescription = '';

    if (score >= 12 && score <= 45) {
      //enviar alerta de posible incidente a Seguridad Publica
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado: "),
            content: Text("$quizScore / 5"),
            actions: <Widget>[
              FlatButton(
                  child: Text("Hacer Denuncia"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Tab3()));
                  }),
              FlatButton(
                child: Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado: $resultString"),
            content: Text("$quizScore / 5"),
            actions: <Widget>[
              FlatButton(
                child: Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }
}
