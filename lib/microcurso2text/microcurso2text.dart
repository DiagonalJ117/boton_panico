import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';


const String _textObjetivo = """

Este curso breve tiene como objetivo que analices las causas y consecuencias de la violencia contra las mujeres.

También, sensibilizarte respecto de las diversas situaciones que atraviesan las mujeres que están en esa situación y conocer las opciones para construir una vida libre de violencia.


""";

const String _text1 = """

Se define como violencia todo acto de agresión física, psicológica, económica y sexual, basada en la superioridad de un sexo sobre otro.

Se considera como violencia a la mujer cualquier acción u omisión, que cause muerte, daño o sufrimiento psicológico, físico, patrimonial, económico o sexual en la mujer, según lo define la _Ley de acceso de las mujeres a una vida libre de violencia para el estado de Sonora_.


""";

const String _text11 = """

Según la Organización de las Naciones Unidas (ONU), “la violencia contra las mujeres es probablemente la violación de los derechos humanos más habitual y que afecta un mayor número de personas…”

Es un problema grave que aqueja a la población y limita su potencial de desarrollo. 

Uno de los problemas que más secuelas deja en una  persona es ser víctima de un delito. El impacto físico y emocional es a veces irreparable. 


""";


const String _text12 = """

__Internacionales__

  - Una de cada tres mujeres en el mundo es golpeada, obligada a mantener relaciones sexuales o sometida a algún otro tipo de abusos a lo largo de su vida.

  - 70% de las mujeres asesinadas mueren a manos de su compañero.

  - 80% de las víctimas de armas ligeras en el mundo son mujeres, niños y niñas.

  - Una de cada tres mujeres en el planeta ha sufrido abusos a manos de agentes del Estado, su familia o conocidos.

  - La violencia de familia es la __primera causa de muerte__ y de minusvalía para muchas mujeres.



""";

const String _text122 ="""

__Nacionales y estatales__

""";

const String _text13 = """

Sucede por los diferentes sistemas que están involucrados en la educación, entre los que podemos mencionar los siguientes:

### __Familia__
La familia es pieza clave en la socialización de las personas y en la inclusión de roles y atributos de género. Desde su nacimiento las expectativas familiares son diferentes según el sexo.

### __Escuela__
Las instituciones del Sistema Educativo son una fuente de reproducción de la desigualdad de género, ya que la información y valores trasmitidos desde la educación preescolar hasta la universitaria, así como los contenidos  de aprendizaje, están impregnados de sexismo.

### __Mercado de trabajo__

En el mercado de trabajo se reproducen los esquemas y modelos de masculinidad y feminidad, a través de:

a)	La feminización de ciertas ocupaciones

b)	Diferencias salariales

c)	Obstáculos para acceder a puestos con poder de decisión

d)	La doble jornada de trabajo que recae sobre las mujeres

### __Los medios de comunicación y el lenguaje__

e)	A través de las palabras o del discurso, se refleja nuestra concepción del mundo y al mismo tiempo, se encasillan las imágenes de las personas y los grupos sociales.

""";


const String _text132 = """

### __Tradiciones culturales__

Las tradiciones que prevalecen en la mayoría de los hogares mexicanos, refrendan la división sexual del trabajo, que asigna a la mujer los papeles de crianza, cuidado de hijas e hijos y la realización de las tareas domésticas. Al hombre se le asignan las funciones de proveedor económico.

### __Estado__

Es una institución fundamental en la organización del poder que, a través de estructuras, establece las reglas que ordenan el intercambios social y define las políticas para distribuir los recursos y generar el bienestar entre la población.

""";

const String _text2 = """
### __Física__

Este tipo de violencia es la más evidente ante el daño que es visible en el cuerpo femenino. Su espectro varía desde un pellizco hasta la muerte. Esta violencia puede ser ejercida, incluso, con objetos. Es necesario mencionar que este tipo de violencia vulnera considerablemente la integridad emocional de la mujer.


### __Emocional__

Constituye una de la formas de violencia más sutil de agresión no visible a primera vista. Su identificación es la más difícil de percibir  por la supuesta “ausencia de evidencias “.

La violencia emocional está presente en todas las categorías de violencia, “es la única que puede presentarse de manera aislada, es por ello la importancia de su especificidad”.

Este tipo de violencia constituye en sí un proceso real de destrucción moral que puede conducir a la enfermedad mental o incluso al suicidio.


""";

const String _text2_2 = """
### __Sexual__

El informe mundial sobre la violencia y la salud define a la violencia sexual como “todo acto sexual, la tentativa de consumar un acto sexual, los comentarios o insinuaciones sexuales no deseados, o las acciones para comercializar o utilizar de cualquier otro modo la sexualidad de una persona mediante coacción por otra persona, independientemente de la relación de ésta con la víctima, en cualquier ámbito, incluidos el hogar y el lugar de trabajo".

Se incluye:

- El uso de fuerza física.

- Las tentativas de obtener sexo bajo coacción.

- La agresión mediante órganos sexuales. 

- El acoso sexual incluyendo la humillación sexual. 

- El matrimonio o cohabitación forzados incluyendo el matrimonio de menores.

- La prostitución forzada y comercialización de mujeres.

- El aborto forzado.

- La denegación del derecho a hacer uso de la anticoncepción o a adoptar medidas de protección contra enfermedades.

- Los actos de violencia que afecten a la integridad sexual de las mujeres tales como la mutilación genital femenina y las inspecciones para comprobar la virginidad.


""";


const String _text21 = """

### __Familiar__
Es el acto abusivo de poder u omisión intencional dirigido a dominar, someter, controlar o agredir de manera física, verbal, psicológica, patrimonial, económica y sexual a las mujeres, dentro o fuera del domicilio familiar. En este caso, el agresor tiene o ha tenido una relación de parentesco por consanguinidad o afinidad, de matrimonio, concubinato o una relación de hecho.

### __Violencia Laboral y Docente__
Se ejerce por las personas que tienen un vínculo laboral, docente o análogo con la víctima, independientemente de la relación jerárquica. Consiste en un acto u omisión en abuso de poder, que daña la autoestima, salud, integridad, libertad y seguridad de la víctima e impide su desarrollo y atenta contra la igualdad. Puede consistir en un solo evento dañino o en una serie de eventos cuya suma produce el daño. También, incluye el acoso o el hostigamiento sexual.


### __Violencia en la Comunidad__
Son los actos individuales o colectivos que transgreden derechos fundamentales de las mujeres y propician su denigración, discriminación, marginación o exclusión en el ámbito público.

""";

const String _text21_2 = """

### __Violencia Institucional__
Son los actos u omisiones de las y los servidores públicos de cualquier orden de gobierno, que discriminen o tengan como fin dilatar, obstaculizar o impedir el goce y ejercicio de los derechos humanos de las mujeres, así como su acceso al disfrute de políticas públicas destinadas a prevenir, atender, investigar, sancionar y erradicar los diferentes tipos de violencia.


### __Violencia Feminicida__
Es la forma extrema de violencia de género contra las mujeres, producto de la violación de sus derechos humanos, en los ámbitos público y privado, conformada por el conjunto de conductas misóginas que pueden conllevar impunidad social y del Estado y puede culminar en homicidio y otras formas de muerte violenta de mujeres.

""";

const String _text22 ="""

En todo tipo de lugares: la casa, la calle, la escuela, el centro de trabajo, etc.

""";


const String _text23 ="""

- Piropo
- Miradas
- Nalgadas
- Hostigamiento sexual
- Groserías amenazas
- Control del dinero
- Celotipia
- Matrimonios forzados
- Trata de mujeres
- Prostitución forzosa
- Trabajo forzado
- Esterilización forzosa
- Abuso sexual
- Violación
- Golpes
- Mutilación
- Asesinato


""";

const String _text4 = """

| Mitos                                                                                       | Realidades                                                                                                                                                                                                                                                                                                                                                                                  |
|---------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| “Las mujeres buscan hombres que ejercen violencia.”                                         | Estas mujeres suelen poseer convicciones rígidas en relación al papel de la mujer y su subordinación al hombre; piensan que con paciencia y tolerancia ayudarán a mejorarlo, al no lograr el cambio, callan por vergüenza.                                                                                                                                                                  |
| “A la mujer le agrada la  violencia, si no, abandonaría a su pareja.”                       | A ninguna mujer le agrada vivir en violencia, por otros motivos no abandonan a sus parejas, ya sean emocionales, sociales o económicos.                                                                                                                                                                                                                                                     |
| “¿Por qué aguantó tanto tiempo y ahora se queja?”                                           | Las mujeres denuncian en promedio después de siete años de sufrir violencia. No es fácil denunciar, están las creencias mencionadas.                                                                                                                                                                                                                                                        |
| “La violencia es un problema de las clases más bajas.”                                      | En la práctica, se conoce que esta realidad traspasa las clases sociales, por lo que no es atribuible a factores derivados de la estructura socioeconómica.                                                                                                                                                                                                                                 |
| “Quien te quiere te pega.”                                                                  | Muchas veces estos patrones se aprenden en el hogar, en dónde la violencia es la forma de interacción conocida. Además, se vincula a los celos que sorprende como explicación que se dan las propias mujeres, al encontrar en ellas una vivencia culposa de base.                                                                                                                           |
| “Las personas educadas y cultas no son violentas.”                                          | Cualquier mujer puede ser víctima de violencia como consecuencia de la cultura de dominación y control de la mujer, y esta no dependerá de la educación formal que se tenga, sino más bien de la educación familiar y social.                                                                                                                                                               |
| “Se trata de casos aislados”                                                                | Las estadísticas de prevalencia indican que a nivel mundial una de cada diez mujeres es o ha sido agredida por su pareja. Este mito es un intento por negar la verdad y no hacerse cargo, al no considerarlo un problema social.                                                                                                                                                            |
| “La violencia en la familia es habitualmente ocasionada por el alcohol y las drogas.”       | Existen alcohólicos que no utilizan la violencia, como hay personas que son violentas y no ingieren alcohol. Comúnmente el agresor que ha ingerido alcohol no violenta a cualquier persona sino a su mujer, actuando en ello los factores sociales mencionados.                                                                                                                             |
| “La violencia en la familia es un asunto privado y se debe arreglar al interior del hogar.” | Vivir y desarrollarse en un ambiente libre de violencia es un derecho esencial de todo ser humano. Asimismo es deber del Estado resguardar este derecho.                                                                                                                                                                                                                                    |
| “Los agresores son enfermos mentales.”                                                      | Menos del 10% de las situaciones de violencia están ocasionados por trastornos psiquiátricos.                                                                                                                                                                                                                                                                                            |
| “¿Cómo se las va a arreglar sola?”                                                          | En la actualidad las mujeres pueden desarrollarse laboralmente a la par que los hombres. El que la mujer no busque empleo muchas veces se debe a que la pareja se lo impide, como otra forma de ejercer poder sobre ella y generar dependencia. Esto se vuelve un círculo vicioso en dónde ella misma llega a creer lo que él le dice, en relación a que no se podrá mantener por sí misma. |
| “La violencia es algo innato, que pertenece a la esencia del ser humano“                    | Actualmente se considera que la violencia es una conducta aprendida a través de patrones familiares y sociales, lo cual es de particular importancia en lo referido a la violencia del hombre contra la mujer, ya que este hombre, por lo general, no es un hombre violento en el resto de sus relaciones.                                                                                  |
| “La mujer es golpeada porque no cumple con sus obligaciones.”                               | Esta creencia se sustenta en estereotipos sexuales rígidos, basados en la creencia que la mujer debiera dedicarse de forma absoluta al hogar y al marido, y como forma de justificar la violencia.                                                                                                                                                                                          |
""";


const String _text5 = """
- Lesiones físicas como enfermedades de trasmisión sexual, mutilaciones, pérdida de la audición, vista, abortos, etc.

- Síntomas y malestares psicosomáticos como dificultad para dormir, dolores de cabeza, espalda, enfermedades gastrointestinales, etc.

- Malestar emocional como depresión, baja autoestima, ansiedad, temor permanente, estrés, suicidios, etc.

- Trastornos de conducta en niñas y niños.

- Favorece el aprendizaje de patrones agresivos en la niñez que probablemente repetirán en la edad adulta. 

- El desarrollo comunitario y social de un país también se ve afectado  ya que disminuye la capacidad productiva de las mujeres, abandono de empleos y ocasiona gasto público. 

""";

const String _text6 = """

Y recuerda, si estás en una o más de las situaciones aquí descritas puedes contactar al Instituto Sonorense de las Mujeres a través de esta app.

""";



class Microcurso2Intro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('Microcurso Empoderamiento de la Mujer',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _textObjetivo,))
        ],
      ),
    );
  }
}

class Microcurso2Cap1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1. Conceptualización de la violencia',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text1,))
        ],
      ),
    );
  }
}


class Microcurso2Cap11 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: 
      Column(
        children: <Widget>[
          SafeArea(child: 
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.1. Antecedentes',
                style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold))
          ]),

          ),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text11,))
        ],
      ),
    );
  }
}

class Microcurso2Cap12 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.2. Estadísticas internacionales, nacionales y estatales.',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text12,))
        ],
      ),
    );
  }
}


class Microcurso2Cap12_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.2. Estadísticas internacionales, nacionales y estatales.',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text122,)),
          Image.asset('assets/Picture6.png')
        ],
      ),
    );
  }
}

class Microcurso2Cap12_3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.2. Estadísticas internacionales, nacionales y estatales.',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text122,)),
          Image.asset('assets/Picture7.png')
        ],
      ),
    );
  }
}

class Microcurso2Cap12_4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.2. Estadísticas internacionales, nacionales y estatales.',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text122,)),
          Image.asset('assets/Picture8.png')
        ],
      ),
    );
  }
}


class Microcurso2Cap13 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.3. Causas de la violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text13,)),
        ],
      ),
    );
  }
}

class Microcurso2Cap13_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('1.3. Causas de la violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text132,)),
        ],
      ),
    );
  }
}


class Microcurso2Cap2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2. Tipos de violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text2,)),
        ],
      ),
    );
  }
}

class Microcurso2Cap2_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2. Tipos de violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text2_2,)),
        ],
      ),
    );
  }
}

class Microcurso2Cap21 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2.1. Modalidades de violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text21,)),
        ],
      ),
    );
  }
}

class Microcurso2Cap21_2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2.1. Modalidades de violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text21_2,)),
        ],
      ),
    );
  }
}


class Microcurso2Cap22 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Flexible(child: 
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2.2. Ámbitos de ocurrencia de la violencia contra las mujeres.',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),)
          ])),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text22,)),
        ],
      ),
    );
  }
}

class Microcurso2Cap23 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('2.3. Manifestaciones de la violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text23)),
        ],
      ),
    );
  }
}


class Microcurso2Cap3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('3. Circulo de la violencia',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          Image.asset('assets/Picture9.png')
        ],
      ),
    );
  }
}



class Microcurso2Cap4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      SingleChildScrollView(child:
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('4. Mitos respecto a la violencia contra las mujeres',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text4)),
        ],
      ),
    ));
  }
}

class Microcurso2Cap5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      SingleChildScrollView(child:
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('5. Consecuencias',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text5)),
        ],
      ),
    ));
  }
}


class Microcurso2Cap6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
    
    Container(
      padding: EdgeInsets.all(15.0),
      child: 
      SingleChildScrollView(child:
      Column(
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text('6. ¿Dónde busco ayuda?',
                style: TextStyle(fontWeight: FontWeight.bold),)
          ]),
          SizedBox(height: 20),
          SafeArea(child: MarkdownBody(data: _text6)),
        ],
      ),
    ));
  }
}