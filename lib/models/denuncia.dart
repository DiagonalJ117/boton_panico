import 'package:firebase_database/firebase_database.dart';

class Denuncia{
  String _id;
  String _razon;
  String _pregunta1;
  String _pregunta2;
  List<String> _ofensas;
  String _idAgresor;
  String _posicion;
  String _userId;

  Denuncia(this._id, this._userId, this._razon, this._pregunta1, this._pregunta2, this._ofensas, this._idAgresor, this._posicion);

  String get userId => _userId;
  String get razon => _razon;
  String get pregunta1 => _pregunta1;
  String get pregunta2 => _pregunta2;
  List<String> get ofensas => _ofensas;
  String get idAgresor => _idAgresor;
  String get posicion => _posicion;


  Denuncia.fromSnapshot(DataSnapshot snapshot) : 
    _id = snapshot.key,
    _userId = snapshot.value["userId"],
    _razon = snapshot.value["razon"],
    _pregunta1 = snapshot.value["pregunta1"],
    _pregunta2 = snapshot.value["pregunta2"],
    _ofensas = snapshot.value["ofensas"],
    _posicion = snapshot.value["posicion"],
    _idAgresor = snapshot.value["idAgresor"];

  toJson(){
    return{
      "userId": _userId,
      "razon": _razon,
      "pregunta1": _pregunta1,
      "pregunta2": _pregunta2,
      "ofensas": _ofensas,
      "posicion": _posicion,
      "idAgresor": _idAgresor,
    };
  }
}